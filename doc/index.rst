.. django-smelly-tokens documentation master file, created by
   sphinx-quickstart on Sat May  6 12:41:07 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to django-smelly-tokens's documentation!
================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   README
   CHANGELOG


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
